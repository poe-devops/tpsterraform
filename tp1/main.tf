terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "nginx" {
    name = "nginx:latest"
    keep_locally = false
}

resource "docker_container" "nginx" {
    image = docker_image.nginx.latest
    name = var.container_name
    ports {
        internal = 80
        external = var.container_port
    } 
}

resource "null_resource" "exec_command"{
  provisioner "local-exec" {
    command = "docker exec ${docker_container.nginx.id} /bin/sh -c 'mkdir test && cd test && touch test.txt && cd .. && apt-get update && apt-get install -y nano'"
  }
  depends_on = [docker_container.nginx]
}  
