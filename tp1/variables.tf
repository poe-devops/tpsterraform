variable "container_name" {
  default     = "my_nginx"
}

variable "container_port" {
  default     = 8082
}
