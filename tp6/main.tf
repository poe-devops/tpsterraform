module "mywebserver" {
    source = "./modules/nginx"
}

module "mysqlserver" {
    source = "./modules/mysql"
}
