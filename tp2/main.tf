resource "docker_image" "nginx" {
    name = "nginx:latest"
    keep_locally = false
}

resource "docker_image" "apache" {
    name = "httpd:latest"
    keep_locally = false
}

resource "docker_container" "nginx" {
    image = docker_image.nginx.latest
    name = var.container_name_nginx
    ports {
        internal = 80
        external = var.container_port_nginx
    } 
    mounts {
      type = "bind"
      source = abspath("./nginx")
      target = "/usr/share/nginx/html"
    }
}

resource "docker_container" "apache" {
    image = docker_image.apache.latest
    name = var.container_name_apache
    ports {
        internal = 80
        external = var.container_port_apache
    } 
    mounts {
      type = "bind"
      source = abspath("./apache")
      target = "/usr/local/apache2/htdocs"
    }
}


