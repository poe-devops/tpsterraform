variable "container_name_nginx" {
  default     = "my_nginx"
}

variable "container_port_nginx" {
  default     = 8083
}

variable "container_name_apache" {
  default     = "my_apache"
}

variable "container_port_apache" {
  default     = 8084
}
