output "container_ip_adresse_nginx"{
    value = docker_container.nginx.ip_address
} 

output "container_ip_adresse_apache"{
    value = docker_container.apache.ip_address
} 