resource "docker_image" "mysqlserver" {
    name = "mysql:latest"
}

resource "docker_container" "mysqlserver" {
    image = docker_image.mysqlserver.latest
    name = var.container_name_mysqlserver
    ports {
        internal = 3306
        external = var.container_port_mysqlserver
    } 
    env = [
    "MYSQL_ROOT_PASSWORD=${var.mysql_root_password}",
    "MYSQL_DATABASE=${var.mysql_database}",
    "MYSQL_USER=${var.mysql_user}",
    "MYSQL_PASSWORD=${var.mysql_password}"
  ]
}
