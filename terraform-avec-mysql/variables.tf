variable "container_name_mysqlserver" {
  default     = "my_sqlserver"
}

variable "container_port_mysqlserver" {
  default     = 3306
}

variable "mysql_root_password" {
  description = "The password for the MySQL root user"
  type        = string
  sensitive   = true
  default = "root"
}

variable "mysql_database" {
  description = "The name of the MySQL database"
  type        = string
  default = "mydatabase"
}

variable "mysql_user" {
  description = "The MySQL user"
  type        = string
  default = "mina"
}

variable "mysql_password" {
  description = "The password for the MySQL user"
  type        = string
  sensitive   = true
  default = "root"
}